"""M3C 2018 Homework 1
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import convolve
from mpl_toolkits.mplot3d import Axes3D

def simulate1(N,Nt,b,e):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0
    
    #initialise padded grids, kernels and edge_management
    [S_c,S_m,pad_null,k0,k0_grid,k0_grid_inv,k1] = initialise(S)
    fc = np.zeros(Nt+1)
    
    #iterate through the years and remember fc
    for year in range(0,Nt+1):
        [S_c,S_m] = run_year(S_c,S_m,pad_null,k0,k0_grid,k0_grid_inv,k1,b,e)
        fc[year] = np.sum(S_c[1:(N+1),1:(N+1)])/(N*N)
    
    #remove padding from S_c to create S
    S = S_c[1:(N+1),1:(N+1)]

    return(S,fc)

def initialise(S):
    """Initialise the input for the simulation. As we want to maintain S's
    dimensions during convolution we need to pad the edges with zeros. Any 
    'leakage' from the convolution into this padding needs to be reset - this
    is what pad_null is for. S is split into binary grids Sc and Sm as this
    is more efficient than continually calculating (1-S) for M cells. Also
    pre-make the convolution kernels k0 and k1 for neighbour and community 
    counting respectively. Finally k0_grid and its inverse k0_grid_inv allow an
    edge-adjusted mean to be found after convolution
    """
    
    #create a matrix for removing edge values post-convolution
    pad_null = np.pad(np.ones(np.shape(S)),(1,1), 'constant', constant_values=(0, 0))
    
    #create padded matrix
    S_c = np.pad(S,(1,1),'constant',constant_values=(0, 0))
    S_m = np.pad(1-S,(1,1),'constant',constant_values=(0, 0))
    
    #create edge matrix and kernel for neighbours
    k0 = np.array([[1,1,1],[1,0,1],[1,1,1]])
    k0_grid = pad_null*(convolve(pad_null,k0))
    k0_grid_inv = pad_null*(1/convolve(pad_null,k0))
    
    #create edge matrix and kernel for community
    k1 = np.array([[1,1,1],[1,1,1],[1,1,1]])
    
    return(S_c,S_m,pad_null,k0,k0_grid,k0_grid_inv,k1)
    
def run_year(S_c,S_m,pad_null,k0,k0_grid,k0_grid_inv,k1,b,e):
    """Runs an iteration of the simulation following the recipe in the 
    question sheet: find number of M and C neighbours -> calculate the points
    for those neigbours and so find fitness for each cell -> find the 
    faction specific and total neighbourhood fitness - > use these values to 
    find the probability of switching factions -> compare to number generator
    to find defectors and update grids. I have kept S split into Sc and Sm for
    effiency's sake rather than calculating this each time.
    """
    
    #find number of C and M neighbours for each village
    number_c = pad_null*convolve(S_c,k0)
    number_m = k0_grid - number_c
    
    #find fitness of C and M neighbours by taking edge-adjusted average
    #multiplying by the original binary matrices S_c and S_m removes
    #fitness values that have 'leaked' from the convolution into neighbouring
    #cells of different factions
    fitness_c = S_c*number_c*k0_grid_inv
    fitness_m = (b*number_c + e*number_m)*k0_grid_inv*S_m
    fitness_total = fitness_c + fitness_m
 
    #find the neighbourhood fitness values for C and total
    neighbourhood_c = convolve(fitness_c,k1)
    neighbourhood_total = convolve(fitness_total,k1)
    
    ##controlling for NaNs, use neighbour fitness to set defection probability
    with np.errstate(divide='ignore'):
        probability_c = neighbourhood_c*np.nan_to_num(1/neighbourhood_total)

    #find the villages switching faction by comparing to random number matrix
    random_matrix = np.random.rand(np.shape(S_c)[0],np.shape(S_c)[1])
    defectors_c = S_c*(probability_c < random_matrix)
    defectors_m = S_m*(probability_c > random_matrix)
    
    #subtract defectors from old factions and add to new factions
    S_c = S_c - defectors_c + defectors_m
    S_m = S_m - defectors_m + defectors_c 
    
    return(S_c,S_m)

def analyze(N,Nt,e,b,Nr):
    """Analyze runs the simulation using the hyperparameters set in the problem 
    sheet. It runs the simulation Nr times for each hyperparameter and stores
    this in the 3D matrix fc_b, where 1st dim is years, 2nd dim is each run and
    3rd dim is the index of that simulations b value.
    """
    fc_b = np.zeros([Nt+1,Nr,np.shape(b)[0]])
    for i,j in enumerate(b):
        for k in range(0,Nr):
            fc_b[:,k,i] = np.transpose(simulate1(N,Nt,j,e)[1])
  
    return(fc_b)
    
def figure2(output,N,Nt,e,b,Nr):
    """This creates an empirical probability mass function for b = 1.2 at its
    half-life point. The binomial nature of the distribution is clearly shown, 
    as the 3x3 starting M population often immediately collapses.
    The next analyses will remove this collapsed population to show the
    true decay of the C population.
    """
    # Figure 2: Empirical probability mass function of b = 1.2
    #First find indices of half-life of C population
    half_life_i = (np.abs(np.mean(output[:,:,6],axis=1)-0.5)).argmin()
    #Use Freedman-Diaconis to find num bins where empirical distribution will
    #closest match theoretical distribution
    UQ = np.percentile(output[half_life_i,:,6],75) 
    LQ = np.percentile(output[half_life_i,:,6],25)
    IQR = UQ - LQ
    opt_bin_size = (2*IQR)/np.cbrt(Nr)
    num_bins = int(1/opt_bin_size)
    #use histogram to bin values and then divide by sum to get PMF, then plot
    [bin_values,bin_edges] = np.histogram(output[half_life_i,:,6],num_bins)
    bin_centres = bin_edges + (bin_edges[1] - bin_edges[0])/2
    bin_centres = np.delete(bin_centres,num_bins)
    bin_values = bin_values / np.sum(bin_values)
    plt.bar(bin_centres,bin_values,2*bin_centres[0],color='k')
    plt.xlabel('Fraction of C at Mean Half-Life')
    plt.ylabel('Empirical Probability (Generated from 1000 Runs)')
    plt.title('Clarke and Ang - figure2 \n Empirical Probability Mass Function of b = 1.2 at Mean Half-Life')
    plt.ylim([0,0.2])
    plt.xlim([0,1])
    plt.savefig('hw12.png')
    
def figure3(output,N,Nt,e,b,Nr):  
    """Figure 3 shows the time to halflife for different values of b. Note
    the exponential decay as b increases linearly from 1. As b closes on 1
    the time to population death increases asymptotically, and even for b=1.01
    we were unable to reach a conclusion after 2000 years. This is reflected in
    the huge variance in time to half-life for these lower b values.
    """
    # Figure 3: Time to half-life of different bs with SD (collapsing M removed)
    half_life_SD = np.zeros([np.shape(b)[0]])
    half_life_mean = np.zeros([np.shape(b)[0]])
    #Iterate through each value of b
    for i in range(len(b)):
        #find runs that had a collapse of M population (max fc = 1)
        M_collapse = np.max(output[:,:,i],axis=0)
        #for every non-M-collapsing run find half-life in years 
        half_lives = np.zeros(np.sum(M_collapse!=1))
        count = 0
        for j in range (0,Nr):
            if M_collapse[j] != 1:
                half_lives[count] = (np.abs(output[:,j,i]-0.5)).argmin()
                count+=1
        #take the mean and SD of half-life across runs
        half_life_mean[i] = np.mean(half_lives)
        half_life_SD[i] = np.std(half_lives)
    #plot mean (SD errorbars) for each b
    plt.errorbar(b,half_life_mean,yerr=half_life_SD,fmt='o',color='k')
    plt.xlabel('Value of b')
    plt.ylabel('Mean Half-Life of C Population in Years (SD)')
    plt.title('Clarke and Ang - figure3 \n Relationship Between Decay of C Population and b Value')
    plt.ylim([0,1200])
    plt.xlim([1,1.32])
    plt.savefig('hw13.png')

def plot_ts_hist(out, b_s): 
    """
    Plots 2 figures: 
    Fig 1 - evolution of fc with time, for different values of b
    Fig 2 - histogram of half-lives of fc, at different values of b. 
    """
    gen, n_iter, n_b = out.shape
    
    avg_fc = np.mean(out, axis=1) # average across all simulations 
    failures = np.any(out==1, axis=0) # runs where mercenaries die out completely
   
    out_2 = np.copy(out)
    out_2[:,failures] = np.nan # removes runs where mercenaries die out
    avg_fc2 = np.nanmean(out_2, axis=1) 
    
    ## Fig 1. Time series plot 
    fig1, (ax1, ax2) = plt.subplots(2, figsize=(10, 15))
    ax1.plot(np.arange(0,gen),avg_fc)
    ax1.legend(["%.2f" % x for x in b_s], loc='upper right', title='b value', bbox_to_anchor=(1.1, 1.0))
    ax1.set_ylabel('Fraction of population that are C')
    ax1.set_xlabel('Year')
    fig1.subplots_adjust(hspace=0.4)
    
    ax2.plot(np.arange(0,gen), avg_fc2)
    ax2.set_ylabel('Fraction of population that are C')
    ax2.set_xlabel('Year')
    
    ax1.set_title('Clarke and Ang - plot_ts_hist() \n Evolution of C population with years')
    ax2.set_title('without runs where M population collapses')
    
    half_life = np.argmax(out<0.5, axis=0)
    
    ## Fig 4. Histogram of fc half-lives 
    fig4 = plt.figure()
    ax3 = fig4.add_subplot(111, projection='3d')
    
    col_scheme = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7']
    for i in np.arange(n_b):
        ys = half_life[:,i]

        hist, bins = np.histogram(ys, bins=20)
        xs = (bins[:-1] + bins[1:])/2
        
        cs = np.array([col_scheme[i]]*len(xs))
        cs[np.where(bins==0)] = 'k' # black bar contains failures - runs where mercernaries die out 

        ax3.bar(xs, hist, zs=b_s[i], zdir='y', width=5.0, color=cs, ec=cs, alpha=0.8)
        
    ax3.set_xlabel('half life of fraction of population that are C')
    ax3.set_ylabel('b value')
    ax3.set_zlabel('frequency')
    ax3.set_title('Clarke and Ang - plot_ts_hist()')
    
    return fig1, fig4

        
if __name__ == '__main__':
    """Define the hyperparameters of the simulation and run. Then take output
    and plot the four graphs of part 2 of the homework sheet
    """

    N = 21 #NxN grid size (odd positive integer greater than 5)
    Nt = 2000 #  number of iterations (years) to run the simulation
    e = 0.01 # number points M villages get for each M neighbour (0<e<<1)
    b = np.array([1.01,1.025,1.05,1.075,1.1,1.15,1.2,1.3])# number points M villages get for C (>1)
    Nr = 1000 # number of iterations per b value

    #output = analyze(N,Nt,e,b,Nr)
    output = np.load('results.npy')

    figure2(output,N,Nt,e,b,Nr)
    figure3(output,N,Nt,e,b,Nr)
    #figure1, figure4 = plot_ts_hist(output, b)

    
    


    
    



