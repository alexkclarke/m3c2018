"""M3C 2018 Homework 1
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import convolve2d
from functools import partial 
import pandas as pd
from ggplot import * # some timestamp error: https://stackoverflow.com/questions/50591982/importerror-cannot-import-name-timestamp


def simulate1(N,Nt,b,e):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0
    
    fc = np.zeros(Nt+1) #Fraction of points which are C
    fc[0] = S.sum()/(N*N)
    
    for gen in range(Nt):
    
        # identify neighbours 
        K = np.array([[1, 1, 1], [1, 0, 1], [1, 1, 1]])
        n_neighbours = convolve2d(np.ones((N,N),dtype=int), K, mode='same')
        n_C = convolve2d(S, K, mode='same') # number of C villages in the neighbourhood
        n_M = n_neighbours - n_C # number of M villages in the neighbourhood
    
        # fitness
        points_C = S*n_C
        points_M = (1-S)*(n_C*b + n_M*e)
        fitness = (points_C + points_M)/n_neighbours
        total_fitness = convolve2d(fitness, np.ones((3,3)), mode='same')
    
        # probability 
        fitness_Cs = S*fitness # returns matrix of fitness of the collaborators
        total_fitness_Cs = convolve2d(fitness_Cs, np.ones((3,3)), mode='same') # total fitness of Cs in the community 
        turn_C_prob = np.nan_to_num(total_fitness_Cs/total_fitness) 
        
        #fitness_Ms = (1-S)*fitness
        #total_fitness_Ms = convolve2d(fitness_Ms, np.ones((3,3)), mode='same') 
        #turn_M_prob = np.nan_to_num(total_fitness_Ms/total_fitness) 
    
        # realization 
        roll_dice = np.random.rand(N,N)
        turn_C = turn_C_prob >= roll_dice
        S = turn_C.astype(int)
        fc[gen+1] = S.sum()/(N*N)
    
    return S,fc
    
    
def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #C locations
    ind_s1 = np.where(S==1) #M locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.hold(True)
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.hold(False)
    plt.show()
    plt.pause(0.05)
    return None


def simulate2(N,Nt,b,e):
    """Simulation code for Part 2, add input variables as needed
    """ 
    S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0
    
    C_contribution = np.zeros(Nt+1)
    M_contribution = np.zeros(Nt+1)#Fraction of points which are C
    
    for gen in range(Nt):
    
        # identify neighbours 
        K = np.array([[1, 1, 1], [1, 0, 1], [1, 1, 1]])
        n_neighbours = convolve2d(np.ones((N,N),dtype=int), K, mode='same')
        n_C = convolve2d(S, K, mode='same') # number of C villages in the neighbourhood
        n_M = n_neighbours - n_C # number of M villages in the neighbourhood
    
        C_to_M = (1-S)*(n_C*(b-1))
        M_to_M = (1-S)*(n_M*e)
        
        C_contribution[gen] = np.sum(C_to_M)/np.sum(1-S)
        M_contribution[gen] = np.sum(M_to_M)/np.sum(1-S)
        
        points_C = S*n_C
        points_M = (1-S)*(n_C*b + n_M*e)
        fitness = (points_C + points_M)/n_neighbours
        total_fitness = convolve2d(fitness, np.ones((3,3)), mode='same')
    
        # probability 
        fitness_Cs = S*fitness # returns matrix of fitness of the collaborators
        total_fitness_Cs = convolve2d(fitness_Cs, np.ones((3,3)), mode='same') # total fitness of Cs in the community 
        turn_C_prob = np.nan_to_num(total_fitness_Cs/total_fitness) 
        
        #fitness_Ms = (1-S)*fitness
        #total_fitness_Ms = convolve2d(fitness_Ms, np.ones((3,3)), mode='same') 
        #turn_M_prob = np.nan_to_num(total_fitness_Ms/total_fitness) 
    
        # realization 
        roll_dice = np.random.rand(N,N)
        turn_C = turn_C_prob >= roll_dice
        S = turn_C.astype(int)
        
    return C_contribution, M_contribution
    
    return


def analyze(b_s):
    """ Add input variables as needed
    """
    N, Nt, e = 21, 150, 0.3
    simulate_b = partial(simulate1, N=N, Nt=Nt, e=e) # new simulate function that has default values for N, Nt and e 
    result = list(map(lambda x: simulate_b(b=x), b_s))
    all_states, all_fc = zip(*result) # unpack
    fc_mat = np.stack(all_fc)
    
    # creating dataframe - just for plotting
    data = pd.DataFrame(fc_mat.T, columns=["%.2f" % b for b in b_s])
    data['generation'] = data.index
    data_lng = pd.melt(data, id_vars='generation', var_name='b', value_name='fc')
    
    print(ggplot(aes(x='generation', y='fc', colour='b'), data=data_lng) + \
    geom_line() + \
    ggtitle ('N = %2d , e = %3.2f' % (N, e)))
    return   
    

if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    
    #N, Nt, b, e = (11, 20, 1.2, 0.5)
    #S, fc = simulate1(N, Nt, b, e)
    c_contribute, m_contribute = simulate2(21, 150, 1.31, 0.3)
    S, fc = simulate1(21, 150, 1.21, 0.3)
    #b_s = np.arange(1.21, 3.01, 0.2) # list of b values to try 
    #analyze(b_s)
    
 

    
    
    
